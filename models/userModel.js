const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const User = new Schema({
    email: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    token: {type: String},
    posts: {type: Array}
})

const UserSchema = mongoose.model('user', User)


module.exports = { UserSchema};