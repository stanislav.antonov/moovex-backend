const mongoose = require('mongoose');
const routes = require('./routes/api');
const authRoutes = require('./routes/auth')
const morgan = require('morgan');
const express = require('express')
const app = express()
const path = require('path')
const dotenv = require('dotenv')
const cors = require('cors');
const authMiddleware = require('./middleware/authMiddleware')


dotenv.config({path: './config/config.env'})

const connectDB = async () => {
    try {
        const conn = await mongoose.connect(process.env.MONGO_URI, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        })
        console.log(`MongoDB Connected: ${conn.connection.host}`)
    } catch (err) {
        console.error(err);
    }
}
connectDB().then((response)=> response);


app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: false }))



app.use(cors());
app.use(express.json())
app.use(morgan('tiny'))
app.use('/api', authMiddleware, routes);
app.use('/auth', authRoutes)

app.listen(process.env.PORT, console.log(`Server is starting at ${process.env.PORT}`))