const {UserSchema} = require ('../models/userModel')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
const {validationResult} = require('express-validator')

const generateAccessToken = (id) => {
    const payload = {
        id
    }
    return jwt.sign(payload,process.env.SECRET_KEY, {expiresIn: "24h"})
}


class authController {
    async registration (req, res) {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: "Error while registration !", errors})
            }
            const {email, password} = req.body
            const candidate = await UserSchema.findOne({email})
            if (candidate) {
                return res.status(400).json({message: 'User with this email already exists !'})
            }
            const hashPassword =  bcrypt.hashSync(password, 7);
            const user = new UserSchema ({email, password: hashPassword})
            await user.save()
            return res.json({message: 'User successfully registered !'})
        } catch (error) {
            console.error(error)
            res.status(400).json({message: `Registration error ! ${error}`})
        }
    }

    async login (req, res) {
        try {
            const {email, password} = req.body
            const user = await UserSchema.findOne({email})
            if (!user) {
                return res.status(401).json({message:`User ${email} wasn't found !` })
            }
            const validPassword = bcrypt.compareSync(password, user.password)

            if (!validPassword) {
                return res.status(401).json({message:"Incorrect password ! Please try again."})
            }
            const token = generateAccessToken(user._id)
            const userData = await UserSchema.findOne({"_id":user._id})
            if (token !== userData.token) {
                await UserSchema.updateOne({"_id":user._id}, {$set: {
                        token: token
                    }} )
            }
            return res.json({token})
        } catch (e) {
            console.error(e)
            res.status(401).json({message: 'Login error !'})
        }
    }
}

module.exports = new authController()