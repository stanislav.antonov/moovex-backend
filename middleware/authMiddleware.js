const jwt = require('jsonwebtoken')

module.exports = function (req, res, next) {
    if (req.method === 'OPTIONS') {
        next()
    }
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (!token) {
            return res.status(403).json({message: "Missing token or user is not authorized !"})
        }
        const decodedData = jwt.verify(token, process.env.SECRET_KEY)
        console.log(decodedData)
        req.user = decodedData
        next()
    } catch (error) {
        return res.status(403).json({message: error.message})
    }
}