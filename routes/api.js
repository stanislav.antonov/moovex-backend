const express = require('express');
const router = express.Router();
const { UserSchema} = require('../models/userModel');

router.get('/',(req, res) => {
    UserSchema.find({}).then((data) => {
        res.json(data)
    }).catch((error) => {
        console.log('error:', error)
    })
})


router.post("/save/:id", (req, res) => {
    console.log("Body", req.body);
    const data = req.body;
    const newUser = new UserSchema(data);

    newUser.save((error) => {
        if (error) {
            res
                .status(500)
                .json({msg: "Ooops ! There was an error while saving your data !"});
            return;
        }
        return res.json({msg: "Your data has been saved"});
    });
});


// router.post('/delete/:id', function (req, res) {
//     const uid = req.params.id.toString();
//
//     UserSchema.deleteOne({"_id": uid}, (error, result) => {
//         if (error) {
//            return  res
//                 .status(500)
//                 .json({msg: "Error while deleting post !"});
//         } else {
//           return   res.json({msg: `User with id ${uid} was updated !`})
//
//         }
//
//     });
// });

router.post('/update/:id',function (req, res) {
    const uid = req.params.id.toString();
    const updateData = {$set: req.body}

    UserSchema.updateOne({"_id": uid}, updateData, (error, result) => {
        if (error) {
            return  res
                .status(500)
                .json({msg: "Error while updating user !"});
        } else {
            return res.json({msg: `User with id ${uid} was updated.`})

        }

    });
});


module.exports = router;