const Router = require('express')
const router = new Router()
const controller = require('../controller/authController')
const {check} = require('express-validator')


router.post('/registration', [
     check('email', "Email field couldn't be empty !").notEmpty(),
    check('password', "Password cannot be more than 4 and less than 10 characters !").isLength({min:4, max:10})
],controller.registration)
router.post ('/login', controller.login)

module.exports = router